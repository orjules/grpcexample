# gRPC in Unity

## Prerequisites
1. Install [NuGetForUnity](https://github.com/GlitchEnzo/NuGetForUnity)
2. Use it to install: `Google.Protobuf`, `Grpc.AspNetCore`, `Grpc.Net.Client` and `Grpc.Tools`

## Compiling protobuf
1. Add .proto file to Assets folder
2. cd into the Assets folder
3. Run `<Path to protoc installation>/protoc -I . --csharp_out=Scripts --grpc_out=Scripts --plugin=protoc-gen-grpc=<Path to protoc installation>/grpc_csharp_plugin Example.proto` to generate a `Example.cs` and `ExampleGrpc.cs`

## Using the generated Code

Add `using Grpc.Net.Client;` to the top of your code.

Make a new Client object:
```
using var channel = GrpcChannel.ForAddress("http://localhost:50051"); // Must be http not https
var client = new Example.ExampleClient(channel);
```

Call a function on the client:
```
var reply = client.getHello(new HelloRq {Name = "Julius"});
Debug.Log("Greeting: " + reply.Answer);
```

## Known issues
When running the program the following error is thrown:
`IOException: The server returned an invalid or unrecognized response.`

This is probably an issue with http1 and http2. 
Refer to https://stackoverflow.com/questions/60360430/why-does-xamarin-android-fails-to-send-grpc-http2-requests/60362990#60362990 for more info.