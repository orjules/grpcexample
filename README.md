# gRPC sample project

This repo shall be used to find a simple workflow, for defining new gRPC server client pairs in C++.

## Prerequisites

1. Install CMake 3.15 or later
2. [Install protobuf](https://grpc.io/docs/languages/cpp/quickstart/)
3. Create the `<Your project>.proto` file and define some functions

## Using the c++ server

1. Change CMakeLists.txt to use your proto files and the name you want (currently example and ex_...)
2. Create the `<Your project>_server.cpp` file and add the main() to it
3. Create the `<Your project>_client.cpp` file
4. Add `-DCMAKE_PREFIX_PATH=<absolute path to installation>` in CLion under "Build, Execution, Deployment/CMAKE/CMake options"
5. Load the CMakeLists. If "ProtobufConfig.cmake" could not be found, the prerequisites where done wrong
6. Build the project
7. Include `<Your project>.grpc.pb.h` in the `<Your project>_server.cpp` file
8. Implement your server (and client)

## Using the c# client
1. `dotnet new console -o <Your project>` will create a new folder with the project name
2. Add the following lines inside the project tags of your `<Your project>.csproj`:
```
<ItemGroup>
    <Protobuf Include="Path to your Proto file" GrpcServices="Client" />
  </ItemGroup>

  <ItemGroup>
    <PackageReference Include="Google.Protobuf" Version="3.22.3" />
    <PackageReference Include="Grpc.AspNetCore" Version="2.40.0" />
    <PackageReference Include="Grpc.Net.Client" Version="2.52.0" />
    <PackageReference Include="Grpc.Tools" Version="2.54.0">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>
  </ItemGroup>
```
3. Add the following to you `Program.cs` file:
```
using System.Threading.Tasks;
using Grpc.Net.Client;

// Must be http not https, but the IP address and port can be changed (and must be the same as on the server)
using var channel = GrpcChannel.ForAddress("http://localhost:50051"); 
```
4. Make a new Client and call a function on it, like the following example. Exchange all the `Example` with the name of your project. Also note the capitalization which is automatically done for C#. 
```
var client = new Example.ExampleClient(channel);
var reply = client.getHello(new HelloRq {Name = "Example"});
Console.WriteLine("Greeting: " + reply.Answer);
```
5. Start the server
6. cd into your new folder and call `dotnet run`