﻿using System.Threading.Tasks;
using Grpc.Net.Client;

// The port number must match the port of the gRPC server.
using var channel = GrpcChannel.ForAddress("http://localhost:50051"); // Must be http not https
var client = new Example.ExampleClient(channel);
var reply = client.getHello(new HelloRq {Name = "Julius"});
Console.WriteLine("Greeting: " + reply.Answer);
Console.WriteLine("Press any key to exit...");
Console.ReadKey();