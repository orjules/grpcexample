//
// Created by Julius Gilka-Bötzow on 25.04.23.
//

#include <grpcpp/grpcpp.h>
#include "Example.grpc.pb.h"

using namespace grpc;

class ExampleClient {
private:
    std::unique_ptr<Example::Stub> stub_;
public:
    ExampleClient(std::shared_ptr<ChannelInterface> channel) : stub_(Example::NewStub(channel)){}

    std::string getHello(const std::string& user) {
        HelloRq request;
        request.set_name(user);

        HelloRsp reply;
        ClientContext context;

        Status status = stub_->getHello(&context, request, &reply);

        if (status.ok()) {
            return reply.answer();
        } else {
            std::cout << status.error_code() << ": " << status.error_message() << std::endl;
            return "RPC failed";
        }
    }

    void getRepeatedHello(const std::string& user){
        HelloRq request;
        request.set_name(user);
        HelloRsp reply;
        ClientContext context;

        std::unique_ptr<ClientReader<HelloRsp> > reader(stub_->getRepeatedHello(&context, request));
        while (reader->Read(&reply)) {
            std::cout << reply.answer() << std::endl;
        }
        Status status = reader->Finish();
    }
};

int main(int argc, char** argv) {
    ExampleClient greeter(grpc::CreateChannel("192.168.178.92:50051", grpc::InsecureChannelCredentials()));
    std::string user("world");
    std::string reply = greeter.getHello(user);
    std::cout << "Greeter received: " << reply << std::endl;
    // greeter.getRepeatedHello("world");



    return 0;
}