// Standard gRPC includes
#include <grpc/grpc.h>
#include <grpcpp/security/server_credentials.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

// Your gRPC include
#include "Example.grpc.pb.h"

// Other things you might need
#include <iostream>

using namespace grpc;

class ExampleServerImpl final : public Example::Service{
    Status getHello(ServerContext* context, const HelloRq* request, HelloRsp* response) override{
        response->set_answer("Hello " + request->name());
        return Status::OK;
    }

    Status getRepeatedHello(ServerContext* context, const HelloRq* request, ServerWriter<HelloRsp>* writer) override {
        HelloRsp response = HelloRsp();
        response.set_answer("Hello " + request->name());
        while (true) {
            writer->Write(response);
        }
        return Status::OK;
    }
};

void RunServer() {
    std::string server_address("0.0.0.0:50051");
    ExampleServerImpl service;

    ServerBuilder builder;
    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *synchronous* service.
    builder.RegisterService(&service);
    // Finally assemble the server.
    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;

    // Wait for the server to shutdown. Note that some other thread must be
    // responsible for shutting down the server for this call to ever return.
    server->Wait();
}

int main(int argc, char** argv) {
    RunServer();

    return 0;
}